/**
 * Thanks user207064 https://stackoverflow.com/a/31586010/1372424
 * Licensed under CC-BY-SA 3.0
 * Modifications made by Subin Siby <subinsb.com>, 2020
 */

package com.varnamproject.pack.ml;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;

public class VarnamProvider extends ContentProvider {
    private static String TAG = "VarnamProvider";

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() {
        // Initialize on startup.
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // TODO: Implement this to handle query requests from clients.
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    /**
     *
     */
    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        Log.v(TAG, "AssetsGetter: Open asset file " + uri.toString());
        AssetManager am = getContext().getAssets();

        String fileName = uri.getLastPathSegment();

        if (fileName == null)
            throw new FileNotFoundException();

        AssetFileDescriptor afd = null;
        try {
            afd = am.openFd(fileName);
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return afd;
    }
}
