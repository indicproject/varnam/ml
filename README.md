# Varnam Language Pack

Note: This repo is based on [this repo](https://gitlab.com/indicproject/varnam/ml), general changes from there should be merged here too.

## Setup

* `git submodule update --init`
* Update assets from the submodule :
```
python3 update-assets.py
```
* Open project in Android Studio and build

## To change the language

To fork this and make app for a new language :
* Edit `update-assets.py` and change the value of scheme variable
* Follow [these steps](https://stackoverflow.com/a/39701758/1372424) to rename package : `com.varnamproject.pack.<lang>`
* Change `strings.xml`
* Change package name in `AndroidManifest.xml`